# peppypedia
* * *
  peppypedia
      reworked version (0.0.2) (current [release](https://github.com/WindowsMeosu/peppypedia-old/releases/tag/v.0.2-beta.6.7))
    
  <p>Please use peppypedia-old's github for the actual version date, the changelog will be updated on 12/22/2022.</p>
      
  * Note: the date is determined by last commit (excluding minor commits) and not determined by when the page was created
   
   * * *



| users: | https://windowsmeosu.github.io/peppypedia/content/en/users/peppy |
| --- | --- |

| users: | https://windowsmeosu.github.io/peppypedia/content/en/users/roles/gmt/Zallius |
| --- | --- |

| users: | https://windowsmeosu.github.io/peppypedia/content/en/users/jhlee0133 |
| --- | --- |

| users: | https://windowsmeosu.github.io/peppypedia/content/en/users/WhiteCat |
| --- | --- |

| maps: | https://windowsmeosu.github.io/peppypedia/content/en/maps/disco_prince |
| --- | --- |

| modes: | [https://windowsmeosu.github.io/peppypedia/content/en/modes/osu!](https://windowsmeosu.github.io/peppypedia/content/en/modes/osu!) |
| --- | --- |

| 10/24/2022: | user/peppy |
| --- | --- |

| 10/24/2022: | user/roles/gmt/Zallius |
| --- | --- |


| 11/11/2022: | user/jhlee0133 |
| --- | --- |

| 11/11/2022: | user/WhiteCat |
| --- | --- |

| 11/03/2022: | user/roles/gmt/deleted |
| --- | --- |


| 10/22/2022: | maps/disco_prince |
| --- | --- |

| 10/29/2022: | modes/osu! |
| --- | --- |



Themes:
* <s>peppypediaplus (WIP, unused for now)</s> (scrapped)
* peppyqq (WIP, unused)
* minima (on peppypedia-old)
