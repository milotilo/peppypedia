---
X-UA-Compatible: [IE=9]
---
# Supported browsers
A list of most supported browsers for peppypedia:
* Mircosoft Edge
* Internet Explorer 9-11 (Anything below 9 is not recommended)
* Google Chrome/Chromium
* Mozilla Firefox
* GNU IceCat
* Any modern browser
